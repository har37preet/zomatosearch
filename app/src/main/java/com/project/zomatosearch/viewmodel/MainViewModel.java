package com.project.zomatosearch.viewmodel;

import android.text.Editable;
import android.util.Log;
import android.widget.Toast;

import androidx.databinding.ObservableField;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.project.zomatosearch.MainActivity;
import com.project.zomatosearch.R;
import com.project.zomatosearch.activity.BaseVMActivity;
import com.project.zomatosearch.activity.BaseViewModel;
import com.project.zomatosearch.adapter.RestaurantRecyclerAdapter;
import com.project.zomatosearch.data.NetworkObserver;
import com.project.zomatosearch.databinding.ActivityMainBinding;
import com.project.zomatosearch.model.Restaurant;
import com.project.zomatosearch.model.SearchResponseModel;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import io.reactivex.Observable;

public class MainViewModel extends BaseViewModel {
    public ObservableField<String> searchQuery = new ObservableField<>("");
    Timer timer;
    private RecyclerView rvRestaurants;
    ActivityMainBinding mainBinding;
    private RestaurantRecyclerAdapter restaurantRecyclerAdapter;


    public void init(ViewDataBinding binding) {
        Log.d("","");
        mainBinding = (ActivityMainBinding)binding;
        rvRestaurants = mainBinding.rvRestaurants;
        rvRestaurants.setLayoutManager(new LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false));
        rvRestaurants.setHasFixedSize(true);
        restaurantRecyclerAdapter = new RestaurantRecyclerAdapter();
        rvRestaurants.setAdapter(restaurantRecyclerAdapter);
    }

    public MainViewModel(BaseVMActivity activity) {
        super(activity);
    }

    public void afterTextChanged(Editable arg0) {
        // user typed: start the timer
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (searchQuery.get().length() >= 3) {
                    searchRestaurants();
                }
            }
        }, 600);
    }

    private void searchRestaurants() {
        mDataManager.login(searchQuery.get())
                .compose(mActivity.bindToLifecycle())
                .subscribe(new NetworkObserver<SearchResponseModel>(mActivity) {
                    @Override
                    protected void onHandleSuccess(SearchResponseModel loginResponse) {
                        super.onHandleSuccess(loginResponse);
                        List<Restaurant> restaurants = groupByCusinie(loginResponse.getRestaurants());
                        restaurantRecyclerAdapter.setRestaurants(restaurants);
                    }
                });
    }

    private List<Restaurant> groupByCusinie(List<Restaurant> restaurants) {
        List<Restaurant> regroup = new ArrayList<>();
        List<String> cusinies = new ArrayList<>();
        for(int i=0;i<restaurants.size();i++){
            restaurants.get(i).getRestaurant().setCusinieList();
            cusinies.addAll(restaurants.get(i).getRestaurant().getCusinieList());
        }
        Log.d("Cusinies List: ", cusinies.toString());
        cusinies = new ArrayList<String>(new HashSet<String>(cusinies));
        Log.d("Cusinies List: ", cusinies.toString());
        for(int i=0;i<cusinies.size();i++){
            regroup.add(new Restaurant(cusinies.get(i)));
            for(int j=0;j<restaurants.size();j++){
                if(restaurants.get(j).getRestaurant().getCusinieList().indexOf(cusinies.get(i))>-1){
                    regroup.add(restaurants.get(j));
                }
            }
        }
        return regroup;
    }

    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (timer != null) {
            timer.cancel();
        }
    }
}