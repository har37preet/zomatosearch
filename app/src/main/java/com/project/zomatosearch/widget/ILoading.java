package com.project.zomatosearch.widget;

public interface ILoading {
    void show();

    void hide();

    void dismiss();
}