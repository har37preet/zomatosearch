package com.project.zomatosearch;

import androidx.databinding.ViewDataBinding;

import android.os.Bundle;

import com.facebook.stetho.Stetho;
import com.project.zomatosearch.activity.BaseVMActivity;
import com.project.zomatosearch.viewmodel.MainViewModel;

public class MainActivity extends BaseVMActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Stetho.initializeWithDefaults(this);
        MainViewModel mainViewModel = new MainViewModel(this);
        ViewDataBinding binding = binding(R.layout.activity_main, mainViewModel);
        mainViewModel.init(binding);
//        ApiInterface apiService =
//                ApiClient.getClient().create(ApiInterface.class);
//
//        Call<SearchResponseModel> call = apiService.getRestaurantsBySearch("waffles");
//        call.enqueue(new Callback<SearchResponseModel>() {
//            @Override
//            public void onResponse(Call<SearchResponseModel> call, Response<SearchResponseModel> response) {
//                Log.d("","");
//            }
//
//            @Override
//            public void onFailure(Call<SearchResponseModel> call, Throwable t) {
//                Log.d("","");
//            }
//        });
    }
}
