package com.project.zomatosearch.activity;

import android.content.Context;

import com.project.zomatosearch.data.DataManager;


/**
 * Created by leon on 2017/3/11.
 */

public class BaseViewModel {
    protected BaseActivity mActivity;
    protected DataManager mDataManager = DataManager.getInstance();

    public BaseViewModel(BaseVMActivity activity) {
        mActivity = activity;
    }

}