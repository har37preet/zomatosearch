package com.project.zomatosearch.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;

import com.project.zomatosearch.BR;
import com.project.zomatosearch.R;
import com.project.zomatosearch.databinding.LayoutRestaurantItemBinding;
import com.project.zomatosearch.model.Restaurant;
import com.project.zomatosearch.model.ResturantModel;

import java.util.ArrayList;
import java.util.List;

public class RestaurantRecyclerAdapter extends RecyclerView.Adapter<RestaurantRecyclerAdapter.RestaurantHolder> {

    private List<Restaurant> restaurants;

    public void setRestaurants(List<Restaurant> restaurants) {
        this.restaurants = restaurants;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RestaurantHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ViewDataBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                        R.layout.layout_restaurant_item, parent, false);
        return new RestaurantHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RestaurantHolder holder, int position) {
        Restaurant obj = restaurants.get(position);
        holder.bind(obj, position);
    }

    @Override
    public int getItemCount() {
        return restaurants==null?0:restaurants.size();
    }

    public class RestaurantHolder extends RecyclerView.ViewHolder {

        private final ViewDataBinding binding;

        public RestaurantHolder(ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(Object obj, int index) {
            binding.setVariable(BR.restaurant, obj);
//            binding.setVariable(BR.viewModel, mViewModel);
//            binding.setVariable(BR.index, index);
            binding.executePendingBindings();
        }

    }
}
