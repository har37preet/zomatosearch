package com.project.zomatosearch.model;

import android.widget.ImageView;

import androidx.databinding.BindingAdapter;
import androidx.room.ColumnInfo;
import androidx.room.PrimaryKey;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.annotations.SerializedName;
import com.project.zomatosearch.R;

import java.util.ArrayList;
import java.util.List;

public class ResturantModel {

    @SerializedName("has_online_delivery")
    private int has_online_delivery;

    @SerializedName("has_table_booking")
    private int has_table_booking;

    @SerializedName("thumb")
    private String thumb;


    @SerializedName("average_cost_for_two")
    private String average_cost_for_two;
    @SerializedName("menu_url")
    private String menu_url;
    @SerializedName("price_range")
    private String price_range;
    @SerializedName("switch_to_order_menu")
    private String switch_to_order_menu;
    @SerializedName("photos")
    private Photos[] photos;
    @SerializedName("all_reviews_count")
    private String all_reviews_count;
    @SerializedName("is_table_reservation_supported")
    private String is_table_reservation_supported;
    @SerializedName("timings")
    private String timings;
    @SerializedName("currency")
    private String currency;
    @SerializedName("opentable_support")
    private String opentable_support;

    @PrimaryKey
    @ColumnInfo(name = "id")
    @SerializedName("id")
    private String id;
    @SerializedName("user_rating")
    private UserRatingModel user_rating;
    @SerializedName("offers")
    private String[] offers;
    @SerializedName("apikey")
    private String apikey;
    @SerializedName("is_delivering_now")
    private String is_delivering_now;
    @SerializedName("deeplink")
    private String deeplink;
    @SerializedName("is_zomato_book_res")
    private String is_zomato_book_res;
    @SerializedName("establishment")
    private String[] establishment;
    @SerializedName("store_type")
    private String store_type;
    @SerializedName("featured_image")
    private String featured_image;
    @SerializedName("url")
    private String url;
    @SerializedName("cuisines")
    private String cuisines;
    @SerializedName("phone_numbers")
    private String phone_numbers;
    @SerializedName("photo_count")
    private String photo_count;
    @SerializedName("highlights")
    private String[] highlights;
    @SerializedName("events_url")
    private String events_url;
    @SerializedName("name")
    private String name;
    @SerializedName("location")
    private LocationModel location;
    @SerializedName("book_again_url")
    private String book_again_url;
    @SerializedName("is_book_form_web_view")
    private String is_book_form_web_view;
    @SerializedName("book_form_web_view_url")
    private String book_form_web_view_url;
    @SerializedName("mezzo_provider")
    private String mezzo_provider;
    @SerializedName("photos_url")
    private String photos_url;

    private List<String> cusinieList;
    private String cusineHeader;

    public List<String> getCusinieList() {
        return cusinieList;
    }

    public void setCusinieList(List<String> cusinieList) {
        this.cusinieList = cusinieList;
    }

    public String getCusineHeader() {
        return cusineHeader;
    }

    public void setCusineHeader(String cusineHeader) {
        this.cusineHeader = cusineHeader;
    }
    public void setCusinieList() {
        if(this.cuisines!=null){
            String[] temp = this.cuisines.split(",");
            for(int i=0;i<temp.length;i++){
                if(this.cusinieList==null){
                    this.cusinieList = new ArrayList<>();
                }
                this.cusinieList.add(temp[i]);
            }
        }
    }

    public boolean isHas_online_delivery() {
        return has_online_delivery==1;
    }

    public void setHas_online_delivery(int has_online_delivery) {
        this.has_online_delivery = has_online_delivery;
    }

    public boolean isHas_table_booking() {
        return has_table_booking==1;
    }

    public void setHas_table_booking(int has_table_booking) {
        this.has_table_booking = has_table_booking;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public String getAverage_cost_for_two() {
        return average_cost_for_two;
    }

    public void setAverage_cost_for_two(String average_cost_for_two) {
        this.average_cost_for_two = average_cost_for_two;
    }

    public String getMenu_url() {
        return menu_url;
    }

    public void setMenu_url(String menu_url) {
        this.menu_url = menu_url;
    }

    public String getPrice_range() {
        return price_range;
    }

    public void setPrice_range(String price_range) {
        this.price_range = price_range;
    }

    public String getSwitch_to_order_menu() {
        return switch_to_order_menu;
    }

    public void setSwitch_to_order_menu(String switch_to_order_menu) {
        this.switch_to_order_menu = switch_to_order_menu;
    }

    public Photos[] getPhotos() {
        return photos;
    }

    public void setPhotos(Photos[] photos) {
        this.photos = photos;
    }

    public String getAll_reviews_count() {
        return all_reviews_count;
    }

    public void setAll_reviews_count(String all_reviews_count) {
        this.all_reviews_count = all_reviews_count;
    }

    public String getIs_table_reservation_supported() {
        return is_table_reservation_supported;
    }

    public void setIs_table_reservation_supported(String is_table_reservation_supported) {
        this.is_table_reservation_supported = is_table_reservation_supported;
    }

    public String getTimings() {
        return timings;
    }

    public void setTimings(String timings) {
        this.timings = timings;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getOpentable_support() {
        return opentable_support;
    }

    public void setOpentable_support(String opentable_support) {
        this.opentable_support = opentable_support;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public UserRatingModel getUser_rating() {
        return user_rating;
    }

    public void setUser_rating(UserRatingModel user_rating) {
        this.user_rating = user_rating;
    }

    public String[] getOffers() {
        return offers;
    }

    public void setOffers(String[] offers) {
        this.offers = offers;
    }

    public String getApikey() {
        return apikey;
    }

    public void setApikey(String apikey) {
        this.apikey = apikey;
    }

    public String getIs_delivering_now() {
        return is_delivering_now;
    }

    public void setIs_delivering_now(String is_delivering_now) {
        this.is_delivering_now = is_delivering_now;
    }

    public String getDeeplink() {
        return deeplink;
    }

    public void setDeeplink(String deeplink) {
        this.deeplink = deeplink;
    }

    public String getIs_zomato_book_res() {
        return is_zomato_book_res;
    }

    public void setIs_zomato_book_res(String is_zomato_book_res) {
        this.is_zomato_book_res = is_zomato_book_res;
    }

    public String[] getEstablishment() {
        return establishment;
    }

    public void setEstablishment(String[] establishment) {
        this.establishment = establishment;
    }

    public String getStore_type() {
        return store_type;
    }

    public void setStore_type(String store_type) {
        this.store_type = store_type;
    }

    public String getFeatured_image() {
        return featured_image;
    }

    public void setFeatured_image(String featured_image) {
        this.featured_image = featured_image;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCuisines() {
        return cuisines;
    }

    public void setCuisines(String cuisines) {
        this.cuisines = cuisines;
    }

    public String getPhone_numbers() {
        return phone_numbers;
    }

    public void setPhone_numbers(String phone_numbers) {
        this.phone_numbers = phone_numbers;
    }

    public String getPhoto_count() {
        return photo_count;
    }

    public void setPhoto_count(String photo_count) {
        this.photo_count = photo_count;
    }

    public String[] getHighlights() {
        return highlights;
    }

    public void setHighlights(String[] highlights) {
        this.highlights = highlights;
    }

    public String getEvents_url() {
        return events_url;
    }

    public void setEvents_url(String events_url) {
        this.events_url = events_url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocationModel getLocation() {
        return location;
    }

    public void setLocation(LocationModel location) {
        this.location = location;
    }

    public String getBook_again_url() {
        return book_again_url;
    }

    public void setBook_again_url(String book_again_url) {
        this.book_again_url = book_again_url;
    }

    public String getIs_book_form_web_view() {
        return is_book_form_web_view;
    }

    public void setIs_book_form_web_view(String is_book_form_web_view) {
        this.is_book_form_web_view = is_book_form_web_view;
    }

    public String getBook_form_web_view_url() {
        return book_form_web_view_url;
    }

    public void setBook_form_web_view_url(String book_form_web_view_url) {
        this.book_form_web_view_url = book_form_web_view_url;
    }

    public String getMezzo_provider() {
        return mezzo_provider;
    }

    public void setMezzo_provider(String mezzo_provider) {
        this.mezzo_provider = mezzo_provider;
    }

    public String getPhotos_url() {
        return photos_url;
    }

    public void setPhotos_url(String photos_url) {
        this.photos_url = photos_url;
    }

    @BindingAdapter({"bind:featured_image"})
    public static void loadImage(ImageView imageView, String imageURL) {
        RequestOptions requestOptions = new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL).centerCrop();
        Glide.with(imageView.getContext())
                .setDefaultRequestOptions(requestOptions)
                .load(imageURL)
                .into(imageView);
    }
}
