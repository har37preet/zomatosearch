package com.project.zomatosearch.model;

import com.google.gson.annotations.SerializedName;

public class Photos {
    @SerializedName("photo")
    private PhotoModel photo;

    public PhotoModel getPhoto() {
        return photo;
    }

    public void setPhoto(PhotoModel photo) {
        this.photo = photo;
    }
}
