package com.project.zomatosearch.model;

import com.google.gson.annotations.SerializedName;

public class UserModel {
    @SerializedName("profile_deeplink")
    private String profile_deeplink;
    @SerializedName("profile_image")
    private String profile_image;
    @SerializedName("profile_url")
    private String profile_url;
    @SerializedName("foodie_color")
    private String foodie_color;
    @SerializedName("name")
    private String name;

    public String getProfile_deeplink() {
        return profile_deeplink;
    }

    public void setProfile_deeplink(String profile_deeplink) {
        this.profile_deeplink = profile_deeplink;
    }

    public String getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }

    public String getProfile_url() {
        return profile_url;
    }

    public void setProfile_url(String profile_url) {
        this.profile_url = profile_url;
    }

    public String getFoodie_color() {
        return foodie_color;
    }

    public void setFoodie_color(String foodie_color) {
        this.foodie_color = foodie_color;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
