package com.project.zomatosearch.model;

import com.google.gson.annotations.SerializedName;

public class UserRatingModel {
    @SerializedName("aggregate_rating")
    private String aggregate_rating;
    @SerializedName("rating_color")
    private String rating_color;
    @SerializedName("rating_text")
    private String rating_text;
    @SerializedName("votes")
    private String votes;

    public String getAggregate_rating() {
        return aggregate_rating;
    }

    public void setAggregate_rating(String aggregate_rating) {
        this.aggregate_rating = aggregate_rating;
    }

    public String getRating_color() {
        return rating_color;
    }

    public void setRating_color(String rating_color) {
        this.rating_color = rating_color;
    }

    public String getRating_text() {
        return rating_text;
    }

    public void setRating_text(String rating_text) {
        this.rating_text = rating_text;
    }

    public String getVotes() {
        return votes;
    }

    public void setVotes(String votes) {
        this.votes = votes;
    }
}
