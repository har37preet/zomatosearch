package com.project.zomatosearch.model;

import com.google.gson.annotations.SerializedName;

public class LocationModel {
    @SerializedName("zipcode")
    private String zipcode;
    @SerializedName("address")
    private String address;
    @SerializedName("city")
    private String city;
    @SerializedName("locality_verbose")
    private String locality_verbose;
    @SerializedName("latitude")
    private String latitude;
    @SerializedName("locality")
    private String locality;
    @SerializedName("country_id")
    private String country_id;
    @SerializedName("city_id")
    private String city_id;
    @SerializedName("longitude")
    private String longitude;

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getLocality_verbose() {
        return locality_verbose;
    }

    public void setLocality_verbose(String locality_verbose) {
        this.locality_verbose = locality_verbose;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getCountry_id() {
        return country_id;
    }

    public void setCountry_id(String country_id) {
        this.country_id = country_id;
    }

    public String getCity_id() {
        return city_id;
    }

    public void setCity_id(String city_id) {
        this.city_id = city_id;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
}
