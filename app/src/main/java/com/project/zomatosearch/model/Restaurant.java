package com.project.zomatosearch.model;

import com.google.gson.annotations.SerializedName;

public class Restaurant {
    @SerializedName("restaurant")
    private ResturantModel restaurant;

    public boolean isHeader;

    public String restaurantHeader;

    public Restaurant(String header){
        this.isHeader = true;
        this.restaurantHeader = header;
        restaurant = null;
    }
    public Restaurant() {
        this.isHeader = false;
        this.restaurantHeader = null;
    }


    public boolean isHeader() {
        return isHeader;
    }

    public void setHeader(boolean header) {
        isHeader = header;
    }

    public String getRestaurantHeader() {
        return restaurantHeader;
    }

    public void setRestaurantHeader(String restaurantHeader) {
        this.restaurantHeader = restaurantHeader;
    }

    public ResturantModel getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(ResturantModel restaurant) {
        this.restaurant = restaurant;
    }
}
