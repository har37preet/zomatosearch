package com.project.zomatosearch.model;

import com.google.gson.annotations.SerializedName;

public class PhotoModel {
    @SerializedName("thumb_url")
    private String thumb_url;
    @SerializedName("friendly_time")
    private String friendly_time;
    @SerializedName("res_id")
    private String res_id;
    @SerializedName("width")
    private String width;
    @SerializedName("caption")
    private String caption;
    @SerializedName("id")
    private String id;
    @SerializedName("user")
    private UserModel user;
    @SerializedName("url")
    private String url;
    @SerializedName("timestamp")
    private String timestamp;
    @SerializedName("height")
    private String height;

    public String getThumb_url() {
        return thumb_url;
    }

    public void setThumb_url(String thumb_url) {
        this.thumb_url = thumb_url;
    }

    public String getFriendly_time() {
        return friendly_time;
    }

    public void setFriendly_time(String friendly_time) {
        this.friendly_time = friendly_time;
    }

    public String getRes_id() {
        return res_id;
    }

    public void setRes_id(String res_id) {
        this.res_id = res_id;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public UserModel getUser() {
        return user;
    }

    public void setUser(UserModel user) {
        this.user = user;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }
}
