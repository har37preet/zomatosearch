package com.project.zomatosearch.data;

import com.project.zomatosearch.model.Restaurant;
import com.project.zomatosearch.model.SearchResponseModel;
import com.project.zomatosearch.utils.RxUtil;

import io.reactivex.Observable;

/**
 * Created by leon on 2017/3/11.
 */

public class DataManager {
    private static DataManager mDataManager;
    private IRemoteDataSource mRemoteDataSource;
    private ILocalDataSource mLocalDataSource;

    private DataManager(IRemoteDataSource remoteDataSource, ILocalDataSource localDataSource) {
        mRemoteDataSource = remoteDataSource;
        mLocalDataSource = localDataSource;
    }

    public static DataManager getInstance() {
        if (mDataManager == null) {
            synchronized (DataManager.class) {
                if (mDataManager == null) {
                    mDataManager = new DataManager(ApiClient.create(), new LocalDataSource());
                }
            }
        }
        return mDataManager;
    }

    public Observable<SearchResponseModel> login(String searchQuery) {
        return mRemoteDataSource.getRestaurantsBySearch(searchQuery).compose(RxUtil.applyScheduler())
                .map(RxUtil.unwrapResponse(null));
    }

//    private <T> Observable<T> preProcess(Observable<BaseResponse<T>> observable) {
//        return observable.compose(RxUtil.applyScheduler())
//                .map(RxUtil.unwrapResponse(null));
//    }
//
//    private <T> Observable<T> preProcess(Observable<BaseResponse<T>> observable, Class<T> cls) {
//        return observable.compose(RxUtil.applyScheduler())
//                .map(RxUtil.unwrapResponse(cls));
//    }
//
//    private Observable<EmptyResponse> preEmptyProcess(Observable<BaseResponse<EmptyResponse>> observable) {
//        return preProcess(observable, EmptyResponse.class);
//    }
//
//
//    public Observable<LoginResponse> login(LoginRequest loginRequest) {
//        return preProcess(mRemoteDataSource.login(loginRequest));
//    }
//
//    public Observable<List<HomeTestBean>> getTestList() {
//        return preProcess(mRemoteDataSource.getTestList());
//    }
//
//    public Observable<EmptyResponse> getEmpty() {
//        return preEmptyProcess(mRemoteDataSource.getEmpty());
//    }
}
