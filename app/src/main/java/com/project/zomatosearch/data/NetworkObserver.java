package com.project.zomatosearch.data;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.HttpException;
import com.project.zomatosearch.activity.BaseActivity;
import com.project.zomatosearch.utils.RxUtil;
import com.project.zomatosearch.widget.ILoading;

import java.net.ConnectException;
import java.net.SocketTimeoutException;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class NetworkObserver<T> implements Observer<T> {
    private static final String TAG = "NetworkObserver";
    private Context mContext;
    private ILoading mLoading;

    public NetworkObserver(Context context) {
        mContext = context;
    }

    public NetworkObserver(Context context, @Nullable ILoading loading) {
        mContext = context;
        mLoading = loading;
    }

    public NetworkObserver(BaseActivity activity) {
        mContext = activity;
        mLoading = activity;
    }

    public NetworkObserver(BaseActivity activity, Boolean showLoading) {
        mContext = activity;
        if (showLoading) {
            mLoading = activity;
        }
    }

    @Override
    public void onSubscribe(Disposable d) {
        if (mLoading != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    mLoading.show();
                }
            });

        }
    }

    @Override
    public void onNext(T result) {
        onHandleSuccess(result);
    }

    @Override
    public void onError(Throwable e) {

        if (mLoading != null) {
            mLoading.hide();
        }

        String errorMsg = e.getMessage();

        if (e instanceof ConnectException) {
            errorMsg = "Connection error";
        } else if (e instanceof HttpException) {
            int errorCode = ((HttpException) e).code();
            errorMsg = "Something went wrong";
        } else if (e instanceof SocketTimeoutException) {
            errorMsg = "Socket Timerout";
        } else {
            errorMsg = e.getMessage();
        }

        onHandleError(errorMsg);
        onHandleFinal();
    }

    @Override
    public void onComplete() {
        if (mLoading != null) {
            mLoading.hide();
        }
        onHandleFinal();
    }

    protected void onHandleSuccess(T t) {}

    protected void onHandleError(String msg) {
        if (msg == null) {
            msg = "Something went wrong";
        }
        Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
    }

    protected void onHandleFinal() {

    }
}