package com.project.zomatosearch.data;

import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import com.project.zomatosearch.BuildConfig;
import com.project.zomatosearch.data.IRemoteDataSource;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {

    public static IRemoteDataSource create() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();

        OkHttpClient client = builder.addInterceptor(chain -> {
            Request.Builder requestBuilder = chain.request().newBuilder();
            requestBuilder.addHeader("user-key", "bf97790948508817d8e453a278efea7a")
                    .addHeader("Accept", "application/json");
            return chain.proceed(requestBuilder.build());
        })
                .connectTimeout(IRemoteDataSource.TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(IRemoteDataSource.READ_TIMEOUT, TimeUnit.SECONDS)
                .addNetworkInterceptor(new StethoInterceptor())
                .build();

        Gson gson = new GsonBuilder().serializeNulls().create();

        return new Retrofit.Builder()
                .client(client)
                .baseUrl(IRemoteDataSource.BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
                .create(IRemoteDataSource.class);
    }


}
