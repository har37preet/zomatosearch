package com.project.zomatosearch.data;

import com.project.zomatosearch.BuildConfig;
import com.project.zomatosearch.model.SearchResponseModel;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface IRemoteDataSource {
    String BASE_URL = "https://developers.zomato.com/api/v2.1/";
    int TIMEOUT = 10;
    int READ_TIMEOUT = 10;

    @GET("search")
    Observable<SearchResponseModel> getRestaurantsBySearch(@Query("q") String searchQuery);
}